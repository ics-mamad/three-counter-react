import React, { useState } from "react";


const Counter = () => {
  const [cnt1, setCnt1] = useState(0);
  const [state1, setState1] = useState();
  const [cnt2, setCnt2] = useState(0);
  const [state2, setState2] = useState();
  const [cnt3, setCnt3] = useState(0);
  const [state3, setState3] = useState();

  // counter-1

  const increment1 = () => {
    setCnt1((cnt1) => cnt1 + 1);
  };
  const counter1 = () => {
    setState1(setInterval(increment1, 400));
  };
  const stop1 = () => {
    if (cnt1 != 0) {
      clearInterval(state1);
    } else {
      alert("Start counter before Stop");
    }
  };
  const resume1 = () => {
    if (cnt1 != 0) {
      setState1(setInterval(increment1, 400));
    } else {
      alert("Can't Resume Before Start");
    }
  };
  const reset1 = () => {
    if(cnt1==0){
      alert("Counter Is Already At 0")
    }else{
    clearInterval(state1);
    setCnt1(0);
  }
  };

  //Counter-2

  const increment2 = () => {
    setCnt2((cnt2) => cnt2 + 1);
  };
  const counter2 = () => {
    setState2(setInterval(increment2, 400));
  };
  const stop2 = () => {
    if (cnt2 != 0) {
      clearInterval(state2);
    } else {
      alert("Start counter before Stop");
    }
  };
  const resume2 = () => {
    if (cnt2 != 0) {
      setState2(setInterval(increment2, 400));
    } else {
      alert("Can't Resume Before Start");
    }
  };
  const reset2 = () => {
    if(cnt2==0){
      alert("Counter Is Already At 0")
    }else{
    clearInterval(state2);
    setCnt2(0);
  }
  };

  //Counter-3

  const increment3 = () => {
    setCnt3((cnt3) => cnt3 + 1);
  };
  const counter3 = () => {
    setState3(setInterval(increment3, 400));
  };
  const stop3 = () => {
    if (cnt3 != 0) {
      clearInterval(state3);
    } else {
      alert("Start counter before Stop");
    }
  };
  const resume3 = () => {
    if (cnt3 != 0) {
      setState3(setInterval(increment3, 400));
    } else {
      alert("Can't Resume Before Start");
    }
  };
  const reset3 = () => {
    if(cnt3==0){
      alert("Counter Is Already At 0")
    }else{
      clearInterval(state3);
      setCnt3(0); 
    }
    }
   


  //General buttons

  const genstart = () =>{
    counter1()
    counter2()
    counter3()
  }
  const genstop = () =>{
    stop1()
    stop2()
    stop3()
  }
  const genresume = () =>{
    resume1()
    resume2()
    resume3()
  }
  const genreset = () =>{
    reset1()
    reset2()
    reset3()
  }

  return (
    <div className="App">
      {cnt1}
      <button className="btn" onClick={counter1}>Start</button>
      <button className="btn" onClick={stop1}>Stop</button>
      <button className="btn" onClick={resume1}>Resume</button>
      <button className="btn" onClick={reset1}>Reset</button>
      <br />
      <br />
      {cnt2}
      <button className="btn" onClick={counter2}>Start</button>
      <button className="btn" onClick={stop2}>Stop</button>
      <button className="btn" onClick={resume2}>Resume</button>
      <button className="btn" onClick={reset2}>Reset</button>
      <br />
      <br />
      {cnt3}
      <button className="btn" onClick={counter3}>Start</button>
      <button className="btn" onClick={stop3}>Stop</button>
      <button className="btn" onClick={resume3}>Resume</button>
      <button className="btn" onClick={reset3}>Reset</button>
      <br />
      <br />
      <br />
      <button className="btn" onClick={genstart}>Start</button>
      <button className="btn" onClick={genstop}>Stop</button>
      <button className="btn" onClick={genresume}>Resume</button>
      <button className="btn" onClick={genreset}>Reset</button>
    </div>
  );
};
export default Counter;
